#!/usr/bin/env python
#-*- coding: iso-8859-1 -*-

'Funciones y procedimientos útiles.'

# -----------------------------------------------------------------------------
# Módulo: Utiles - Version 0.02 - 01/09/09
# Carlos Zayas Guggiari <carlos@carloszayas.com>
# -----------------------------------------------------------------------------

import os, sys

# -----------------------------------------------------------------------------
# Funciones
# -----------------------------------------------------------------------------

def leer(archivo):

    'Retorna una lista con el contenido del archivo especificado.'

    return open(archivo).readlines()

# -----------------------------------------------------------------------------

def camino():

    'Retorna la carpeta de trabajo actual.'

    return os.getcwd()

# -----------------------------------------------------------------------------

def raiz(direccion):

    'Cambia la carpeta de trabajo actual.'

    os.chdir(direccion)

# -----------------------------------------------------------------------------

def contenido(direccion):

    'Retorna la raiz, las carpetas y los archivos de una dirección.'

    for raiz, carpetas, archivos in os.walk(direccion):

        return raiz, carpetas, archivos

# -----------------------------------------------------------------------------

def limpiar(cadena):

    'Quita espacios y newlines del final de una cadena.'

    while cadena and (cadena[-1] in [' ','\n']):

        cadena = cadena[:-1]

    return cadena

# -----------------------------------------------------------------------------

def recorrer(direccion, extension = ''):

    'Retorna una lista de archivos, opcionalmente aquellos de cierto tipo.'

    lista = []

    raiz, carpetas, archivos = contenido(direccion)

    if archivos:
        for archivo in archivos:
            lista.append(os.path.join(raiz, archivo))
            if extension:
                if archivo[-len(extension):].lower() != extension:
                    lista.pop()

    if carpetas:
        for carpeta in carpetas:
            lista.extend(recorrer(os.path.join(raiz, carpeta), extension))

    return lista

# -----------------------------------------------------------------------------

def doc(objeto):

    'Retorna la documentación de un objeto.'

    documentacion = []

    for item in dir(objeto):

        documentacion.append([item, eval('objeto.' + item + '.__doc__')])

    return documentacion

# -----------------------------------------------------------------------------

def espacios(cantidad):

    'Retorna una cadena de espacios.'

    return ' ' * cantidad

# -----------------------------------------------------------------------------

try:
    # DOS, Windows
    import msvcrt
    getkey = msvcrt.getch
except ImportError:
    # Se asume Unix
    import tty, termios

    def getkey():

        'Retorna una tecla presionada.'

        file = sys.stdin.fileno()
        mode = termios.tcgetattr(file)

        try:
            tty.setraw(file, termios.TCSANOW)
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(file, termios.TCSANOW, mode)

        return ch

# -----------------------------------------------------------------------------
# Procedimientos
# -----------------------------------------------------------------------------

def guardar(lista, nombre):

    'Guarda una lista en un archivo.'

    archivo = open(nombre, 'w')

    for item in lista:
        archivo.write(item + '\n') # archivo.writelines() no agrega 'newline'.

    archivo.close()

# -----------------------------------------------------------------------------

def clear(l = 100):

    'Borra la pantalla de texto.'

    if os.name == 'posix':
        # Unix, Linux, MacOS, BSD, etc.
        os.system('clear')
    elif os.name in ('nt', 'dos', 'ce'):
        # DOS, Windows
        os.system('CLS')
    else:
        # Otros sistemas operativos
        print '\n' * l

# -----------------------------------------------------------------------------
