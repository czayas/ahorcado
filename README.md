#Ahorcado - Copyleft 2009 Carlos Zayas Guggiari

Ésta es una implementación en Python y en modo consola del clásico juego de adivinanzas. Fue escrito como ejemplo para un curso de introducción a Python.

Al iniciar el programa, el jugador debe elegir una categoría de términos. Cada archivo de texto con extensión .aho corresponde a una categoría.

Ahorcado es multiplataforma, fue probado con éxito tanto en Windows como en diversas distros GNU/Linux. El único requisito es tener Python 2.x instalado.

Recibiré con agrado cualquier comentario en **carlos (at) zayas.org**
