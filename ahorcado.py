#!/usr/bin/env python
#-*- coding: iso-8859-1 -*-

'Ahorcado 1.0 - Carlos Zayas Guggiari - Agosto 2009'

#------------------------------------------------------------------------------
#                            Importación de módulos
#------------------------------------------------------------------------------

import random

from utiles import *

#------------------------------------------------------------------------------
#                       Definición de variables globales
#------------------------------------------------------------------------------

aviso = frase = pista = tecleadas = categoria = ''; fallas = limite = 0

#------------------------------------------------------------------------------
#                           Definición de funciones
#------------------------------------------------------------------------------

def iniciar(reset):

    'Inicializa variables globales.'

    global aviso, frase, pista, tecleadas, categoria, fallas, limite

    aviso = 'Ahorcado 1.0 (Copyleft by Carlos Zayas) Apriete <Esc> para salir.'

    if reset: categoria = menu()  # Si es un nuevo juego, se elige categoría.

    if categoria:
        frase = elegir(categoria) # Frase oculta que el jugador debe completar.
        pista = rayas(frase)      # Cada raya corresponde a una letra o número.
        tecleadas = ''            # Lista de teclas que ya fueron presionadas.
        fallas = 0                # Cantidad de fallas cometidas por el jugador.
        limite = 5                # Cantidad límite de fallas permitidas.
        jugar = True              # Falso si el usuario desea terminar el juego.
    else:
        jugar = False

    return jugar

#------------------------------------------------------------------------------

def menu():

    'Administra el menú de categorías de palabras.'

    global aviso

    lista = categorias()

    if lista:

        clear(); avisar(aviso + '\n\nElija una categoría:\n')
        contador = 0
        opcion = ''

        # Imprime lista de categorías
        for item in lista:
            if contador > 9: continue
            contador += 1
            avisar(str(contador) + ':' + item.title())

        # Solicita al usuario una opción
        while not opcion.isdigit():
            opcion = pedir('\n¿Número?')
            if escape(opcion): opcion = '0'

        opcion = int(opcion)

        if opcion:
            archivo = lista[opcion - 1] + '.aho'
        else:
            archivo = False

    else:

        archivo = False

    return archivo

#------------------------------------------------------------------------------

def dibujar(etapas):

    'Dibuja el ahorcado.'

    if etapas:
        print espacios(8),' _______'
        print espacios(8),
        if etapas >= 1:
            print ' |/  |'
        else:
            print ' |/'
        print espacios(8),
        if etapas >= 2: 
            print ' |   O'
        else:
            print ' |'
        print espacios(8),
        if etapas >= 3: 
            print ' |  /|\\'
        else:
            print ' |'
        print espacios(8),
        if etapas >= 4:
            print ' |   |'
        else:
            print ' |'
        print espacios(8),
        if etapas >= 5:
            print ' |  / \\'
        else:
            print ' |'
        print espacios(8),'/|\\'
        print espacios(8),'---\n'
    else:
        print '\n' * 8

#------------------------------------------------------------------------------

def escape(caracter):

    'Retorna verdadero si el caracter es escape.'

    return ord(caracter) == 27

#------------------------------------------------------------------------------

def categorias():

    'Permite elegir la categoría de palabras.'

    raiz, carpetas, archivos = contenido(camino())

    lista = []

    for item in archivos:
        if item[-4:] == '.aho':
            lista.append(item[:-4])

    return lista

#------------------------------------------------------------------------------

def elegir(archivo):

    'Elige una linea al azar de entre el contenido de un archivo.'

    lista = leer(archivo)

    linea = lista[random.randint(0, lista.__len__() - 1)].upper()

    return linea

#------------------------------------------------------------------------------

def rayas(cadena):

    'Crea la cadena de rayas que se usa como pista para el jugador.'

    pista = ''

    for caracter in cadena:
        if caracter.isalnum():
            pista += '_'
        else:
            pista += caracter

    return pista

#------------------------------------------------------------------------------

def mostrar(cadena):

    'Muestra la cadena que se usa como pista para el jugador.'

    linea = '\n'

    for caracter in cadena:
        linea += ' ' + caracter

    linea += '\n'

    avisar(linea)

#------------------------------------------------------------------------------

def pedir(mensaje):

    'Solicita una tecla al usuario.'

    if mensaje: avisar(mensaje)

    salir = False

    while not salir:
        tecla = getkey().upper()
        salir = tecla.isalnum() or escape(tecla)

    return tecla

#------------------------------------------------------------------------------

def avisar(mensaje):

    'Imprime un mensaje en pantalla.'

    print mensaje

#------------------------------------------------------------------------------

def status(cadena):

    'Imprime el estado del juego.'

    global aviso, categoria, fallas

    clear()         # Limpia la pantalla.
    mostrar(cadena) # Muestra la frase o la pista.
    dibujar(fallas) # Dibuja el ahorcado en base a las fallas.

    if aviso: # Imprime el resto de la información del juego.
        linea = aviso + '\nCategoría: ' + categoria[:-4].title() + ' - ' \
                      + 'Fallas: ' + str(fallas) + ' de ' + str(limite)
        avisar(linea)

#------------------------------------------------------------------------------

def terminar(mensaje):

    'Devuelve True si el jugador quiere salir.'

    status(frase)

    return pedir(mensaje) != 'S'

#------------------------------------------------------------------------------

def main():

    'Función principal.'

    global aviso, frase, pista, tecleadas, categoria, fallas, limite

    jugar = iniciar(True)

    while jugar:

        status(pista)
        aviso = ''
        salir = False

        if pista == frase:                               # Gana la partida.
            salir = True; aviso = '¡Ganaste!'
        elif fallas == limite:                           # ¡Ahorcado!
            salir = True; aviso = '¡Perdiste!'
        else:                                            # Gestión de la jugada.
            tecla = pedir('¿Tecla?')

            if escape(tecla):
                salir = True; aviso = 'Parece que vas a salir.'
            elif tecla in tecleadas:
                aviso = 'Ya usaste la tecla "' + tecla + '" antes.'
            else:
                aviso = 'Presionaste la tecla "' + tecla + '". '

                tecleadas += tecla
                pos = frase.find(tecla)
                fallas += int(pos == -1)

                if pos > -1: 
                    aviso += '¡Acertaste!'
                else:
                    aviso += '¡Cuidado!'

                while pos > -1:
                    pista = pista[0:pos] + frase[pos] + pista[pos+1:]
                    pos = frase.find(tecla, pos + 1)

        if salir:                                        # ¿Continuar o salir?

            jugar = not terminar('¿Empezar un nuevo juego?')
            if jugar: jugar = iniciar(escape(tecla))

    clear(); avisar('\n¡Hasta pronto!\n')

#------------------------------------------------------------------------------
#                      Ejecución de la función principal
#------------------------------------------------------------------------------

if __name__ == '__main__': main()
